import React from "react";
class MyComponent extends React.Component {

    constructor (props){
        super(props);
    }

    render(){
        return <div>My Component</div>;
    }

}
export default MyComponent;